/**
 * Created by Rajesh_Krishnan1 on 2/25/2015.
 */

angular.module('CommonServices', [],function($provide){
    $provide.factory('MasterListService', function($http, $log){
        $log.log("MasterList Service....");
        return {
            getMasterList: function() {
                $log.log("getMasterList....");
                var listResults= {"Open":[{"Client":"Big Basket(BBC)","Role":"Python Developer","RequirementID":"BBC-JNR-PYT","No_of_positions":1,"Req_Open_Date":"","Status":"Offered","Last_Date_Checked_with_Client":"","Posn_CV_Ratio":"","CV's_Required":0,"Min_Salary":900000,"Max_Salary":1100000,"Avg_Salary":1000000,"Subhanu_Svc_Chg":0.0833,"Invoice_%":0.0833,"Service_Tax":0.1236,"Requirement_Sequence":"","Position_Sequence":"","Priority":""},{"Client":"","Role":"QA","RequirementID":"BBC-JNR-QA","No_of_positions":1,"Req_Open_Date":"2015-01-28T00:00:00.000Z","Status":"Open","Last_Date_Checked_with_Client":"","Posn_CV_Ratio":3,"CV's_Required":3,"Min_Salary":600000,"Max_Salary":800000,"Avg_Salary":700000,"Subhanu_Svc_Chg":0.0833,"Invoice_%":0.0833,"Service_Tax":0.1236,"Requirement_Sequence":"","Position_Sequence":"","Priority":""}]};
                var masterListURL = "https://script.google.com/a/macros/subhanu.com/s/AKfycbxADPZw-9MEmlE_SZhOt9DJQHvvt61zFBe-iNtCSK-5ltSfBvh_/exec?id=19-JTgE39ascjRrRWEziDxXmZo96E1vcgUlHREgjGKuI&sheet=Open&callback=JSON_CALLBACK";
                $log.log(masterListURL);
                return $http.jsonp(masterListURL).
                    then(function(response) {
                        console.log("Inside MasterList Service Call.. Status" + response.status);
                        if(response.status == 200){
                            console.log("Inside MasterList Service Call.." + response.data);
                            listResults = response;
                        }
                      return listResults ;
                    });
            }
        };
    });
});
