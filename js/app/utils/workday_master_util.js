/**
 * Created by Rajesh_Krishnan1 on 2/25/2015.
 */

function getWorkdaySummary(workdayEntries, username,fromDate, toDate) {
   //console.log(workdayEntries.length);
   //console.log(fromDate);
   //console.log(toDate);

    var workingDates = {};
    var formattedFromDate = new Date(fromDate);
    var formattedToDate = new Date(toDate);
    var workdaySessions = [];

    var workdaySummary = {
        "Duration_Summary": {
            "Total_Duration_Days": 0,
            "Total_Duration_Hours": 0,
            "Total_Expected_Hours": 0,
            "Total_Working_Days": 0,
            "Total_Weekly_Off": 0,
            "Total_Holidays": 0,
            "Total_Leaves": 0,
            "Total_Leave_Hours": 0,
            "Total_Missed_Punches": 0,
            "Total_Worked_Days": 0,
            "Total_Not_Worked_Days": 0,
            "Total_Worked_Hours": 0,
            "Total_Effective_Hours": 0,
            "Total_Worked_Milliseconds": 0,
            "Total_Late_Days": 0,
            "Total_Late_Hours": 0,
            "Total_Late_Milliseconds": 0,
            "Total_Overtime_Days": 0,
            "Total_Overtime_Hours": 0,
            "Total_Overtime_Milliseconds": 0,
            "Total_Break_Time": 0,
            "Total_Break_Hours": 0,
            "Total_Meeting_Time": 0,
            "Total_Training_Time": 0,
            "Total_Reading_Time": 0,
            "Total_Sadhana_Time": 0,
            "Total_Client_Visit_Time": 0
        },
        WorkDays:workdaySessions
    };


    var noOfElapseDays = dateDiffInDays(formattedFromDate,formattedToDate);
   console.log(" No of Elapse Days" + noOfElapseDays);

    var indexDate = new Date(formattedFromDate.getTime());
    var indexDateStr = "";
    var noOfDurationDays = 0;
/*    for(var i = 0; i < noOfElapseDays; i++) {
        console.log(" formattedFromDate" + formattedFromDate);
        if(i == 0){
            indexDate = new Date(formattedFromDate.getTime());
        } else {
            indexDate = new Date(indexDate.getTime() + 86400000);
        }

        console.log(" indexDate" + indexDate);
        // indexDateStr = indexDate.toLoString().substring(0, 10);
        indexDateStr = indexDate.toLocaleDateString();
        console.log(" indexDateStr" + indexDateStr);
        console.log(" currentDate" + currentDate);

        if (indexDate <= currentDate){
            console.log(" isless");
            workingDates[indexDateStr] = "Not Worked";
            noOfDurationDays++;
            var day = indexDate.getDay();
            if(day == 6){
                workdaySummary.Duration_Summary.Total_Duration_Hours = workdaySummary.Duration_Summary.Total_Duration_Hours + 4;
            } else {
                workdaySummary.Duration_Summary.Total_Duration_Hours = workdaySummary.Duration_Summary.Total_Duration_Hours + 8.5;
            }
        }else {
            console.log(" isMore");
        }

    }*/

    for (var i = 0; i < noOfElapseDays; i++) {

        if(i == 0){
            indexDate = new Date(formattedFromDate.getTime());
        } else {
            indexDate = new Date(indexDate.getTime() + 86400000);
        }
        indexDateStr = indexDate.toLocaleDateString();
        //console.log(" indexDateStr" + indexDateStr);

        workingDates[indexDateStr] = "Not Worked";
        noOfDurationDays++;
        var day = indexDate.getDay();
        if (day == 6) {
            workdaySummary.Duration_Summary.Total_Duration_Hours = workdaySummary.Duration_Summary.Total_Duration_Hours + 6.5;
        } else {
            workdaySummary.Duration_Summary.Total_Duration_Hours = workdaySummary.Duration_Summary.Total_Duration_Hours + 8.5;
        }
    }
    workdaySummary.Duration_Summary.Total_Duration_Days = noOfDurationDays;

    //console.log( workdaySummary.Duration_Summary.Total_Duration_Days);
    //console.log( noOfDurationDays);
    //Start with Total Duration Days
    //Reduce as you find Holidays/Weekly Off
    workdaySummary.Duration_Summary.Total_Working_Days = noOfDurationDays;
/*    for (var x in workingDates)
    {
        var value = workingDates[x];
        console.log(x + "->" + value);
        console.log("\n");
    }*/

    var workdayEntry = "";
    var lastSessionDate = "";
    var lastSessionLogin = "";
    var lastSessionLogout = "";
    var newWorkDaySession = "";
    var lastSessionComment = "";

    for(var i = 0; i < workdayEntries.length; i++) {
       //console.log("Iteration -> " + i);
        workdayEntry = workdayEntries[i];
        var workdateTime = String(workdayEntry.Time);
        var workdate = "";

        if (workdateTime.length < 20) {
            workdate = formatDate(workdayEntry.Time);
        } else {
            workdate = convertGoogleDate(workdayEntry.Time);
        }
       console.log("workdate -> " + workdate);

        //Remove Entries from Selected dates where entries were found
        var checkDate = workdate.toLocaleDateString();
       //console.log("checkDate -> " + checkDate);
        if (checkDate in workingDates) {
            // then, delete it
            delete workingDates[checkDate];
        }

        if (workdayEntry.WORK_TYPE == "Weekly Off") {
            workdaySummary.Duration_Summary.Total_Working_Days = workdaySummary.Duration_Summary.Total_Working_Days - 1;
            workdaySummary.Duration_Summary.Total_Weekly_Off = workdaySummary.Duration_Summary.Total_Weekly_Off + 1;
            if (lastSessionDate != workdate.toLocaleDateString() && lastSessionLogin != ''){
                newWorkDaySession = new workdaySession(lastSessionLogin, lastSessionLogin, "", workdayEntry.BREAK_TIME, "Missing Day Logout " + lastSessionComment, workdayEntry.Comments, workdayEntry.Learning, workdayEntry.Username);
                workdaySessions.push(newWorkDaySession);
            }

            newWorkDaySession = new workdaySession(workdate, "", "", "", workdayEntry.WORK_TYPE, workdayEntry.Comments, workdayEntry.Learning, workdayEntry.Username);
            workdaySessions.push(newWorkDaySession);
            lastSessionDate = "";
            lastSessionLogin = "";
            lastSessionComment = "";


            workdaySummary.Duration_Summary.Total_Duration_Hours = workdaySummary.Duration_Summary.Total_Duration_Hours - 8.5;

        }

        if (workdayEntry.WORK_TYPE == "Holiday") {
            workdaySummary.Duration_Summary.Total_Working_Days = workdaySummary.Duration_Summary.Total_Working_Days - 1;
            workdaySummary.Duration_Summary.Total_Holidays = workdaySummary.Duration_Summary.Total_Holidays + 1;
            if (lastSessionDate != workdate.toLocaleDateString() && lastSessionLogin != ''){
                newWorkDaySession = new workdaySession(lastSessionLogin, lastSessionLogin, "", workdayEntry.BREAK_TIME, "Missing Day Logout " + lastSessionComment, workdayEntry.Comments, workdayEntry.Learning, workdayEntry.Username);
                workdaySessions.push(newWorkDaySession);
            }
            newWorkDaySession = new workdaySession(workdate, "", "", "", workdayEntry.WORK_TYPE, workdayEntry.Comments, workdayEntry.Learning, workdayEntry.Username);
            workdaySessions.push(newWorkDaySession);
            lastSessionDate = "";
            lastSessionLogin = "";
            lastSessionComment = "";

            var day = workdate.getDay();
            if(day == 6){
                workdaySummary.Duration_Summary.Total_Duration_Hours = workdaySummary.Duration_Summary.Total_Duration_Hours - 6.5;
            } else {
                workdaySummary.Duration_Summary.Total_Duration_Hours = workdaySummary.Duration_Summary.Total_Duration_Hours - 8.5;
            }
        }

        if (workdayEntry.WORK_TYPE == "Missed Punch") {
            workdaySummary.Duration_Summary.Total_Missed_Punches = workdaySummary.Duration_Summary.Total_Missed_Punches + 1;
        }

        if (workdayEntry.WORK_TYPE == "Leave") {

            var leave = 0;
            if (workdayEntry.LOGIN_TYPE == 'Half Day') {
                leave = 0.5;
                workdaySummary.Duration_Summary.Total_Leave_Hours = workdaySummary.Duration_Summary.Total_Leave_Hours + 6.5;
            }
            if (workdayEntry.LOGIN_TYPE == 'Full Day') {
                leave = 1;
                workdaySummary.Duration_Summary.Total_Leave_Hours = workdaySummary.Duration_Summary.Total_Leave_Hours + 8.5;
            }
            if (lastSessionDate != workdate.toLocaleDateString() && lastSessionLogin != ''){
                newWorkDaySession = new workdaySession(lastSessionLogin, lastSessionLogin, "", workdayEntry.BREAK_TIME, "Missing Day Logout " + lastSessionComment, workdayEntry.Comments, workdayEntry.Learning, workdayEntry.Username);
                workdaySessions.push(newWorkDaySession);
            }
            workdaySummary.Duration_Summary.Total_Leaves = workdaySummary.Duration_Summary.Total_Leaves + leave;
            newWorkDaySession = new workdaySession(workdate, "", "", "", workdayEntry.LOGIN_TYPE + " " + workdayEntry.WORK_TYPE, workdayEntry.Comments, workdayEntry.Learning, workdayEntry.Username);
            workdaySessions.push(newWorkDaySession);
            lastSessionDate = "";
            lastSessionLogin = "";
            lastSessionComment = "";
        }

        //Total Time
        if (workdayEntry.BREAK_TIME != null) {
            workdaySummary.Duration_Summary.Total_Break_Time = workdaySummary.Duration_Summary.Total_Break_Time + Number(workdayEntry.BREAK_TIME);
        }
        if (workdayEntry.TRAINING_TIME != null) {
            workdaySummary.Duration_Summary.Total_Training_Time = workdaySummary.Duration_Summary.Total_Training_Time + Number(workdayEntry.TRAINING_TIME);
        }

        if (workdayEntry.READING_TIME != null) {
            workdaySummary.Duration_Summary.Total_Reading_Time = workdaySummary.Duration_Summary.Total_Reading_Time + Number(workdayEntry.READING_TIME);
        }

        if (workdayEntry.SADHANA_TIME != null) {
            workdaySummary.Duration_Summary.Total_Sadhana_Time = workdaySummary.Duration_Summary.Total_Sadhana_Time + Number(workdayEntry.SADHANA_TIME);
        }
        if (workdayEntry.CLIENT_VISIT_TIME != null) {
            workdaySummary.Duration_Summary.Total_Client_Visit_Time = workdaySummary.Duration_Summary.Total_Client_Visit_Time + Number(workdayEntry.CLIENT_VISIT_TIME);
        }
        if (workdayEntry.MEETING_TIME != null) {
            workdaySummary.Duration_Summary.Total_Meeting_Time = workdaySummary.Duration_Summary.Total_Meeting_Time + Number(workdayEntry.MEETING_TIME);
        }

        if (workdayEntry.WORK_TYPE == 'Missed Punch' || workdayEntry.WORK_TYPE == 'Working-Office' || workdayEntry.WORK_TYPE == 'Working-Home' || workdayEntry.WORK_TYPE == 'Working-Holiday') {
            if (workdayEntry.LOGIN_TYPE == 'Login' && lastSessionDate == '') {
               //console.log("Entered -> 1");
                lastSessionDate = workdate.toLocaleDateString();
                lastSessionLogin = workdate;
                if (workdayEntry.WORK_TYPE == 'Missed Punch') {
                    lastSessionComment = "Missed Punch - Login";
                }
                if(i == (workdayEntries.length -1)){
                    newWorkDaySession = new workdaySession(lastSessionLogin, lastSessionLogin, "", workdayEntry.BREAK_TIME, "Missing Day Logout " + lastSessionComment, workdayEntry.Comments, workdayEntry.Learning, workdayEntry.Username);
                    workdaySessions.push(newWorkDaySession);
                }

            } else if (workdayEntry.LOGIN_TYPE == 'Login' && lastSessionDate == workdate.toLocaleDateString() && lastSessionLogin != '') {
               //console.log("Entered -> 2");
                if (workdayEntry.WORK_TYPE == 'Missed Punch') {
                    lastSessionComment = "Missed Punch - Login";
                }

                newWorkDaySession = new workdaySession(lastSessionLogin, lastSessionLogin, "", workdayEntry.BREAK_TIME, "Missing Session Logout " + lastSessionComment, workdayEntry.Comments, workdayEntry.Learning, workdayEntry.Username);
                workdaySessions.push(newWorkDaySession);
                lastSessionDate = workdate.toLocaleDateString();
                lastSessionLogin = workdate;
            } else if (workdayEntry.LOGIN_TYPE == 'Login' && lastSessionDate != '' && lastSessionDate != workdate.toLocaleDateString() && lastSessionLogin != '') {
               //console.log("Entered -> 3");
                if (workdayEntry.WORK_TYPE == 'Missed Punch') {
                    lastSessionComment = "Missed Punch - Login";
                }

                newWorkDaySession = new workdaySession(lastSessionLogin, lastSessionLogin, "", workdayEntry.BREAK_TIME, "Missing Day Logout " + lastSessionComment, workdayEntry.Comments, workdayEntry.Learning, workdayEntry.Username);
                workdaySessions.push(newWorkDaySession);
                lastSessionDate = workdate.toLocaleDateString();
                lastSessionLogin = workdate;
            } else if (workdayEntry.LOGIN_TYPE == 'Logout' && lastSessionDate == '') {
               //console.log("Entered -> 4");
                if (workdayEntry.WORK_TYPE == 'Missed Punch') {
                    lastSessionComment = "Missed Punch - Logout";
                }

                newWorkDaySession = new workdaySession(workdate, lastSessionLogin, workdate, workdayEntry.BREAK_TIME, "Missing Login " + lastSessionComment, workdayEntry.Comments, workdayEntry.Learning, workdayEntry.Username);
                workdaySessions.push(newWorkDaySession);
                lastSessionDate = "";
                lastSessionLogin = "";
                lastSessionComment = "";
            } else if (workdayEntry.LOGIN_TYPE == 'Logout' && lastSessionDate == workdate.toLocaleDateString() && lastSessionLogin != '') {
               //console.log("Entered -> 5");
                if (workdayEntry.WORK_TYPE == 'Missed Punch') {
                    lastSessionComment = "Missed Punch - Logout";
                }
                var loggedTime = getLoggedTime(lastSessionLogin,workdate);
                var lateTime = getLateTime(lastSessionLogin);
                var overTime = getOverTime(workdate);
                workdaySummary.Duration_Summary.Total_Worked_Milliseconds = workdaySummary.Duration_Summary.Total_Worked_Milliseconds + Number(loggedTime.loggedTimeInMilliSeconds);
                workdaySummary.Duration_Summary.Total_Late_Milliseconds = workdaySummary.Duration_Summary.Total_Late_Milliseconds + Number(lateTime.lateTimeInMilliSeconds);
                workdaySummary.Duration_Summary.Total_Overtime_Milliseconds = workdaySummary.Duration_Summary.Total_Overtime_Milliseconds + Number(overTime.overTimeInMilliSeconds);

                if(lateTime.lateTimeInMilliSeconds > 0){
                    workdaySummary.Duration_Summary.Total_Late_Days = workdaySummary.Duration_Summary.Total_Late_Days + 1;
                }
                if(overTime.overTimeInMilliSeconds > 0){
                    workdaySummary.Duration_Summary.Total_Overtime_Days = workdaySummary.Duration_Summary.Total_Overtime_Days + 1;
                }

                newWorkDaySession = new workdaySession(lastSessionLogin, lastSessionLogin, workdate, workdayEntry.BREAK_TIME, lastSessionComment, workdayEntry.Comments, workdayEntry.Learning, workdayEntry.Username,loggedTime.loggedTimeInHours,lateTime.lateTimeInHours,overTime.overTimeInHours);
                workdaySessions.push(newWorkDaySession);
                lastSessionDate = "";
                lastSessionLogin = "";
                lastSessionComment = "";
            } else if (workdayEntry.LOGIN_TYPE == 'Logout' && lastSessionDate != workdate.toLocaleDateString() && lastSessionLogin != '') {
               //console.log("Entered -> 6");
                if (workdayEntry.WORK_TYPE == 'Missed Punch') {
                    lastSessionComment = "Missed Punch - Logout";
                }

                newWorkDaySession = new workdaySession(lastSessionLogin, lastSessionLogin, workdate, workdayEntry.BREAK_TIME, lastSessionComment, workdayEntry.Comments, workdayEntry.Learning, workdayEntry.Username);
                workdaySessions.push(newWorkDaySession);
                lastSessionDate = "";
                lastSessionLogin = "";
                lastSessionComment = "";
            }
        }


    }
    for (var x in workingDates)
    {
        //console.log("Key:\n—- " + x + "\n");

        var value = workingDates[x];
         //console.log("Not Worked");
        // console.log(x + "->" + value);
        //console.log("\n");
        newWorkDaySession = new workdaySession(new Date(x),"","","",value,"","","");
        workdaySessions.push(newWorkDaySession);
        workdaySummary.Duration_Summary.Total_Not_Worked_Days = workdaySummary.Duration_Summary.Total_Not_Worked_Days + 1;
        //console.log("Not Worked Days" + workdaySummary.Duration_Summary.Total_Not_Worked_Days);

    }
    //Calculate Summary
    workdaySummary.Duration_Summary.Total_Worked_Days =
        workdaySummary.Duration_Summary.Total_Duration_Days  -
        (workdaySummary.Duration_Summary.Total_Holidays + workdaySummary.Duration_Summary.Total_Weekly_Off +
        workdaySummary.Duration_Summary.Total_Leaves +
        workdaySummary.Duration_Summary.Total_Not_Worked_Days);

    workdaySummary.Duration_Summary.Total_Worked_Hours = getHoursFromMilliseconds(workdaySummary.Duration_Summary.Total_Worked_Milliseconds);
    workdaySummary.Duration_Summary.Total_Late_Hours = getHoursFromMilliseconds(workdaySummary.Duration_Summary.Total_Late_Milliseconds);
    workdaySummary.Duration_Summary.Total_Overtime_Hours = getHoursFromMilliseconds(workdaySummary.Duration_Summary.Total_Overtime_Milliseconds);
    workdaySummary.Duration_Summary.Total_Break_Hours = getHoursFromMilliseconds( Number(workdaySummary.Duration_Summary.Total_Break_Time) * 60000 );
    workdaySummary.Duration_Summary.Total_Effective_Hours = getHoursFromMilliseconds(workdaySummary.Duration_Summary.Total_Worked_Milliseconds - (Number(workdaySummary.Duration_Summary.Total_Break_Time) * 60000) );

    workdaySummary.Duration_Summary.Total_Training_Time = getHoursFromMilliseconds((Number(workdaySummary.Duration_Summary.Total_Training_Time) * 60000) );
    workdaySummary.Duration_Summary.Total_Reading_Time = getHoursFromMilliseconds((Number(workdaySummary.Duration_Summary.Total_Reading_Time) * 60000) );
    workdaySummary.Duration_Summary.Total_Sadhana_Time = getHoursFromMilliseconds((Number(workdaySummary.Duration_Summary.Total_Sadhana_Time) * 60000) );
    workdaySummary.Duration_Summary.Total_Client_Visit_Time = getHoursFromMilliseconds((Number(workdaySummary.Duration_Summary.Total_Client_Visit_Time) * 60000) );
    workdaySummary.Duration_Summary.Total_Meeting_Time = getHoursFromMilliseconds((Number(workdaySummary.Duration_Summary.Total_Meeting_Time) * 60000) );

    workdaySummary.Duration_Summary.Total_Expected_Hours = workdaySummary.Duration_Summary.Total_Duration_Hours - Number(workdaySummary.Duration_Summary.Total_Leave_Hours) ;
   //console.log("Expected Hours " + workdaySummary.Duration_Summary.Total_Expected_Hours);

   //console.log(JSON.stringify(workdaySummary));
    //console.log(JSON.stringify(workdaySessions));
    return workdaySummary;
}

// a and b are javascript Date objects
function dateDiffInDays(a, b) {
        var _MS_PER_DAY = 1000 * 60 * 60 * 24;
    // Discard the time and time-zone information.
    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

    return ((Math.floor((utc2 - utc1) / _MS_PER_DAY)) + 1);
}

function convertGoogleDate(input) {
    // do some bounds checking here to ensure it has that index
   //console.log("convertGoogleDate -> " + input);
    if (input != null) {
        var indexOfFirstBrace = input.indexOf("(");
        var indexOfFirstComma = input.indexOf(",");
        var indexOfSecondComma = input.indexOf(",", indexOfFirstComma + 1);
        var indexOfThirdComma = input.indexOf(",", indexOfSecondComma + 1);
        var indexOfFourthComma = input.indexOf(",", indexOfThirdComma + 1);
        var indexOfFifthComma = input.indexOf(",", indexOfFourthComma + 1);

        var indexOfLastBrace = input.indexOf(")");

        var year = input.substring(indexOfFirstBrace + 1, indexOfFirstComma);
        var month = input.substring(indexOfFirstComma + 1, indexOfSecondComma);
        // console.log("month-> " + month);
        if (month != '') {
            month = Number(month) + 1;
        }
        var day = input.substring(indexOfSecondComma + 1, indexOfThirdComma);

        if (strDate == '//') {
            strDate = '0/0/0';
            //console.log("Blank Date" + strDate);
        }
        var hour = input.substring(indexOfThirdComma + 1, indexOfFourthComma);
        //console.log("Hour-> " + hour);

        var minute = input.substring(indexOfFourthComma + 1, indexOfFifthComma);
        //console.log("minute-> " + minute);

        var second = input.substring(indexOfFifthComma + 1, indexOfLastBrace);
        //console.log("second-> " + second);

        var strDate = month + "/" + day + "/" + year + " " + hour + ":" + minute + ":" + second;
       //console.log("strDate-> " + strDate);

        var formattedDate = new Date(strDate);
        //console.log("formattedDate-> " + formattedDate);
        return formattedDate;
    }
}

function workdaySession (workdate,login,logout,breaktime,systemComments,comments,learning,user,loggedTime,lateTime,overTime){
    this.Workdate = workdate;
    this.Login = login;
    this.Logout = logout;
    this.BreakTime = breaktime;
    this.SystemComments = systemComments;
    this.SessionComments = comments;
    this.SessionLearning = learning;
    this.user = user;
    this.LoggedTime = loggedTime;
    this.LateTime = lateTime;
    this.OverTime = overTime;
}

/*Usage:

    datecompare(data1, '===', data2) for equality check,
    datecompare(data1, '>', data2) for greater check,
    !datecompare(data1, '>', data2) for less or equal check*/

function datecompare(date1, sign, date2) {
    var day1 = date1.getDate();
    var mon1 = date1.getMonth();
    var year1 = date1.getFullYear();
    var day2 = date2.getDate();
    var mon2 = date2.getMonth();
    var year2 = date2.getFullYear();
    if (sign === '===') {
        if (day1 === day2 && mon1 === mon2 && year1 === year2) return true;
        else return false;
    }
    else if (sign === '>') {
        if (year1 > year2) return true;
        else if (year1 === year2 && mon1 > mon2) return true;
        else if (year1 === year2 && mon1 === mon2 && day1 > day2) return true;
        else return false;
    }
}

function formatDate(input) {

    //console.log("Date Formatter -> " + input);
    if (input != null) {
        var indexOfFirstBrace = input.indexOf("(");
        var indexOfFirstComma = input.indexOf(",");
        var indexOfSecondComma = input.indexOf(",", indexOfFirstComma + 1);
        var indexOfLastBrace = input.indexOf(")");

        var year = input.substring(indexOfFirstBrace + 1, indexOfFirstComma);
        var month = input.substring(indexOfFirstComma + 1, indexOfSecondComma);
        // console.log("month-> " + month);
        if (month != '') {
            month = Number(month) + 1;
        }
        var day = input.substring(indexOfSecondComma + 1, indexOfLastBrace);

        var strDate = month + "/" + day + "/" + year;
        //console.log("strDate-> " + strDate);
        if (strDate == '//') {
            strDate = '0/0/0';
            //console.log("Blank Date" + strDate);
        }
        var formattedDate = new Date(strDate);
        //console.log("formattedDate-> " + formattedDate);
        return formattedDate;
    }
}

function getLoggedTime(date1,date2){
    var loggedTime = {
        loggedTimeInMilliSeconds:0,
        loggedTimeInHours:""
    };

    var diff = date2.getTime() - date1.getTime();
    if(diff > 0) {
        loggedTime.loggedTimeInMilliSeconds = diff;
        var hours = Math.floor(diff / (1000 * 60 * 60));
        diff -= hours * (1000 * 60 * 60);
        var mins = Math.floor(diff / (1000 * 60));
        diff -= mins * (1000 * 60);
        var minutes = "";
        if(Number(mins) < 10){
            minutes = "0"+String(mins);
        } else {
            minutes = mins;
        }
        loggedTime.loggedTimeInHours = hours + ":" + minutes;
       //console.log("Logged Time ->" + hours + " hours : " + minutes + " minutes ");
    }
    return loggedTime;
}

function getLateTime(date2){
    var date1 = new Date(date2.valueOf());
    var day = date1.getDay();
    if(day == 6){
        date1.setHours(9,0);
    } else {
        date1.setHours(8,30);
    }
    //date1.setHours(8,30);
   //console.log("Input ->" + date2 );
   //console.log("Login Time ->" + date1 );
    var lateTime = {
        lateTimeInMilliSeconds:0,
        lateTimeInHours:""
    };

    var diff = date2.getTime() - date1.getTime();
    if(diff > 0){
        lateTime.lateTimeInMilliSeconds = diff;
        var hours = Math.floor(diff / (1000 * 60 * 60));
        diff -= hours * (1000 * 60 * 60);
        var mins = Math.floor(diff / (1000 * 60));
        diff -= mins * (1000 * 60);
        var minutes = "";
        if(Number(mins) < 10){
            minutes = "0"+String(mins);
       } else {
            minutes = mins;
        }
        lateTime.lateTimeInHours = hours + ":" + minutes;
       //console.log("Late Time ->" + hours + " hours : " + minutes + " minutes" );
    }
    return lateTime;
}

function getOverTime(date2){
    var date1 = new Date(date2.valueOf());
    var day = date2.getDay();
    if(day == 6){
        date1.setHours(16,0);
    } else {
        date1.setHours(17,30);
    }

    var overTime = {
        overTimeInMilliSeconds:0,
        overTimeInHours:""
    };
   //console.log("Input ->" + date2 );
   //console.log("Logout Time ->" + date1 );
    var diff = date2.getTime() - date1.getTime();
    if(diff > 0) {
        overTime.overTimeInMilliSeconds = diff;
        var hours = Math.floor(diff / (1000 * 60 * 60));
        diff -= hours * (1000 * 60 * 60);
        var mins = Math.floor(diff / (1000 * 60));
        diff -= mins * (1000 * 60);
        var minutes = "";
        if(Number(mins) < 10){
            minutes = "0"+String(mins);
        } else {
            minutes = mins;
        }
        overTime.overTimeInHours = hours + ":" + minutes;
       //console.log("Over Time ->" + hours + " hours : " + minutes + " minutes");
    }
    return overTime;
}

function getHoursFromMilliseconds(diff){

    var timeInHours = "";
    var hours = Math.floor(diff / (1000 * 60 * 60));
    diff -= hours * (1000 * 60 * 60);
    var mins = Math.floor(diff / (1000 * 60));
    diff -= mins * (1000 * 60);
    var minutes = "";
    if(Number(mins) < 10){
        minutes = "0"+String(mins);
    } else {
        minutes = mins;
    }
    timeInHours = hours + ":" + minutes;
   //console.log("Time ->" + hours + " hours : " + minutes + " minutes");
    return timeInHours;
}