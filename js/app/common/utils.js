/**
 * Created by Rajesh_Krishnan on 2/3/2015.
 *
 * Formatting the result returned by Google Visualization API
 */

function formatResult(data){

    var column_length = Object.keys(data.cols).length;
    var noOfRows = Object.keys(data.rows).length;

    console.log("No of Columns -> " + column_length);
    console.log("No of rows -> " + noOfRows);

    if (!column_length || !noOfRows)
    {
        return false;
    }
    var columns = [],
        result = [],
        row_length,
        value;
    for (var column_idx in data.cols)
    {
        columns.push(data.cols[column_idx].label);
    }
    for (var rows_idx in data.rows)
    {
        row_length = data.rows[rows_idx]['c'].length;
        if (column_length != row_length)
        {
            //  we have a problem!
            return false;
        }
        var count = 0;
        for (var row_idx in data.rows[rows_idx]['c'])
        {
            //console.log("count" + count);
            count++;
            if (!result[rows_idx])
            {
                result[rows_idx] = {};
            }
            try{
                //console.log( "Step -> 1");
                value = !!data.rows[rows_idx]['c'][row_idx].v ? data.rows[rows_idx]['c'][row_idx].v : null;
               // console.log( "Step -> 2");
                result[rows_idx][columns[row_idx]] = value;
               // console.log( "Step -> 3");
               // console.log(JSON.stringify([columns[row_idx]]) + " -> " + value );
               // console.log(JSON.stringify(result[rows_idx][columns[row_idx]]));
                //console.log( "Step -> 3a");

            }
            catch(e){
                //console.log( "Step -> 4");
                result[rows_idx][columns[row_idx]] = " ";
               // console.log( "Step -> 5");
               // console.log( "Error -> " + JSON.stringify([columns[row_idx]]));
            }
            //console.log( "Result -> " + JSON.stringify(result));
        }
    }

    var resultStr = "{\"ResultSet\":" + JSON.stringify(result) + "}";
    //console.log(resultStr);
    return JSON.parse(resultStr);
}
