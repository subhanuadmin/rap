/**
 * Created by snehita on 1/9/2016.
 */
function StatusReportCtrl($scope,$window,$rootScope,notify) {
    console.log('StatusReportCtrl');

    $scope.loadDailyStatusReport = function () {
        console.log("Load Daily Status");
        $window.open("https://drive.google.com/drive/u/0/folders/0BxYvrrMah1kAV2J5QklSQ1RyN0k", '_target');

    };

    $scope.loadWeeklyStatusReport = function () {
        console.log("Load Daily Status");
        $window.open("https://drive.google.com/drive/u/0/folders/0BxYvrrMah1kAflpWbmJRbE11UVp5MjJENW85RXZGQmZCYjRybU1nNFdoblVpWHN4dFVBdGM", '_target');

    };

    $scope.loadTaskStatusReport = function () {
        console.log("Load Task Status");
        $window.open("https://docs.google.com/spreadsheets/d/1FKf_yE4OGg0aWb9WsNqiaBUHTeJPJDXw9Out0gVFz9w/edit#gid=123194708", '_target');

    };

    $scope.loadFeedbackStatusReport = function () {
        console.log("Load Feedback Status");
        $window.open("https://docs.google.com/spreadsheets/d/1Rz6ABQGK3JFo3DzTtnaWd8CeJNV4gvhZeRo6VOauQkE/edit#gid=0", '_target');

    };
}