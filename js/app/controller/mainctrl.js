/**
 * Created by Rajesh_Krishnan on 2/24/2015.
 */

/**
 * MainCtrl - controller
 */
function MainCtrl($rootScope,$scope,$window) {

    console.log("Main Called");
    this.userName = 'Subhanu user';
    this.designation = 'Subhanu Designation';
    this.helloText = 'Welcome to Rapidd India';
    this.descriptionText = 'Rapidd Application Pro';
    $rootScope.activeRequirements = null;
    $scope.activateUserName = false;
    $rootScope.loggedInUser = null;

    var stored_token = $window.localStorage.getItem("token");
    console.log("stored_token" + stored_token);
    $rootScope.root_stored_token=stored_token ;
    console.log("happened");

    $scope.$watch('activateUserName', function() {
        // alert('hey, myVar has changed!');
    });


   /* $scope.feedbackColDefs = [{field: 'Feedback_Type', displayName: 'Feedback Type', width: "auto",resizable: true},
        { field: 'Feedback_Category', displayName: 'Category', width: "auto",resizable: true,cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><a ui-sref="index.feedback_details">{{row.getProperty(col.field)}}</a></div>' },
        { field: 'Repeated', displayName: 'Repeated', width: "auto" ,resizable: true},
        { field: 'Feedback', displayName: 'Feedback', width: "auto" ,resizable: true}];*/

   /* $scope.ngOptions2 = {
        data: 'feedbacks',
        columnDefs: 'feedbackColDefs',
        showGroupPanel: true,
        jqueryUIDraggable: true

    };
*/

    $scope.setUser = function(){
        console.log("setUser " + this.user);

        if(this.user == 'snehitha.u'){
            console.log("snehitha.u");
            $scope.userName = 'Snehitha U';
            $scope.displayName = 'Snehitha U';
            $scope.designation = 'Junior software Developer';
            $scope.imageName = 'sne.png';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'snehitha.u@subhanu.com';
            /* $rootScope.isFTE = true;
             $rootScope.trackerURL = "https://docs.google.com/spreadsheets/d/1sghT1lNS_AJqgJUUewiOXvQ8nHj8K6lLwg0Scgfq36I/edit";*/
        }



        if(this.user == 'snehitha.u'){
            console.log("snehitha.u");
            $scope.userName = 'Snehitha U';
            $scope.displayName = 'Snehitha U';
            $scope.designation = 'Junior software Developer';
            $scope.imageName = 'sne.png';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'snehitha.u@subhanu.com';
           /* $rootScope.isFTE = true;
            $rootScope.trackerURL = "https://docs.google.com/spreadsheets/d/1sghT1lNS_AJqgJUUewiOXvQ8nHj8K6lLwg0Scgfq36I/edit";*/
        }

        if(this.user == 'sharath.kumar'){
            console.log("sharath.kumar");
            $scope.userName = 'Sharath Kumar A.S';
            $scope.displayName = 'Sharath Kumar A.S';
            $scope.designation = 'UX/UI Designer';
            $scope.imageName = 'sharath.jpg';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'sharath.kumar@subhanu.com';
            /* $rootScope.isFTE = true;
             $rootScope.trackerURL = "https://docs.google.com/spreadsheets/d/1sghT1lNS_AJqgJUUewiOXvQ8nHj8K6lLwg0Scgfq36I/edit";*/
        }


        if(this.user == 'anandkumar.ks'){
            console.log("anandkumar.ks");
            $scope.userName = 'Anandkumar KS';
            $scope.displayName = 'Anandkumar KS';
            $scope.designation = 'Trainer';
            $scope.imageName = 'anand.jpg';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'anandkumar.ks@subhanu.com';
            /* $rootScope.isFTE = true;
             $rootScope.trackerURL = "https://docs.google.com/spreadsheets/d/1sghT1lNS_AJqgJUUewiOXvQ8nHj8K6lLwg0Scgfq36I/edit";*/
        }

        if(this.user == 'kapil.kushwaha'){
            console.log("kapil.kushwaha");
            $scope.userName = 'Kapil Kushwaha';
            $scope.displayName = 'Kapil Kushwaha';
            $scope.designation = 'Trainee Software Developer';
            $scope.imageName = 'kapil.jpg';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'kapil.kushwaha@subhanu.com';
            /* $rootScope.isFTE = true;
             $rootScope.trackerURL = "https://docs.google.com/spreadsheets/d/1sghT1lNS_AJqgJUUewiOXvQ8nHj8K6lLwg0Scgfq36I/edit";*/
        }
        if(this.user == 'aayush.chaubey'){
            console.log("aayush.chaubey");
            $scope.userName = 'Aayush Chaubey';
            $scope.displayName = 'Aayush Chaubey';
            $scope.designation = 'Trainee Software Developer';
            $scope.imageName = 'aayush.jpg';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'aayush.chaubey@subhanu.com';
            /* $rootScope.isFTE = true;
             $rootScope.trackerURL = "https://docs.google.com/spreadsheets/d/1sghT1lNS_AJqgJUUewiOXvQ8nHj8K6lLwg0Scgfq36I/edit";*/
        }


        if(this.user == 'deepa.p'){
            console.log("deepa.p");
            $scope.userName = 'Deepa P';
            $scope.displayName = 'Deepa P';
            $scope.designation = 'Junior Developer ';
            $scope.imageName = 'deepa.jpg';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'deepa.p@subhanu.com';
            /* $rootScope.isFTE = true;
             $rootScope.trackerURL = "https://docs.google.com/spreadsheets/d/1sghT1lNS_AJqgJUUewiOXvQ8nHj8K6lLwg0Scgfq36I/edit";*/
        }
        if(this.user == 'sandeep.srinivas'){
            console.log("sandeep.srinivas");
            $scope.userName = 'Sandeep Srinivas';
            $scope.displayName = 'Sandeep Srinivas';
            $scope.designation = ' STEM Trainer ';
            $scope.imageName = 'sandeep.jpg';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'sandeep.srinivas@subhanu.com';
            /* $rootScope.isFTE = true;
             $rootScope.trackerURL = "https://docs.google.com/spreadsheets/d/1sghT1lNS_AJqgJUUewiOXvQ8nHj8K6lLwg0Scgfq36I/edit";*/
        }
        if(this.user == 'mayur.shet'){
            console.log("mayur.shet");
            $scope.userName = 'Mayur Shet';
            $scope.displayName = 'Mayur Shet';
            $scope.designation = ' STEM Trainer ';
            $scope.imageName = 'mayur.jpeg';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'mayur.shet@subhanu.com';
            /* $rootScope.isFTE = true;
             $rootScope.trackerURL = "https://docs.google.com/spreadsheets/d/1sghT1lNS_AJqgJUUewiOXvQ8nHj8K6lLwg0Scgfq36I/edit";*/
        }
        if(this.user == 'vasantha.k'){
            console.log("vasantha.k");
            $scope.userName = 'Vasantha';
            $scope.displayName = 'Vasantha';
            $scope.designation = ' STEM Trainer ';
            $scope.imageName = 'vasantha.jpg';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'vasantha.k@subhanu.com';
            /* $rootScope.isFTE = true;
             $rootScope.trackerURL = "https://docs.google.com/spreadsheets/d/1sghT1lNS_AJqgJUUewiOXvQ8nHj8K6lLwg0Scgfq36I/edit";*/
        }

        if(this.user == 'sreenivasulu.h'){
            console.log("sreenivasulu.h");
            $scope.userName = 'Sreenivasulu H';
            $scope.displayName = 'Sreenivasulu H';
            $scope.designation = ' STEM Trainer ';
            $scope.imageName = 'srinivas.jpg';
            $scope.gid = "601975560";
            $rootScope.loggedInUser = $scope.userName;
            $rootScope.userId = 'sreenivasulu.h@subhanu.com';
            /* $rootScope.isFTE = true;
             $rootScope.trackerURL = "https://docs.google.com/spreadsheets/d/1sghT1lNS_AJqgJUUewiOXvQ8nHj8K6lLwg0Scgfq36I/edit";*/
        }

        $scope.welcomeText = 'Jai Gurudev ' + $scope.displayName;
        $scope.welcomeDescription = 'Have a good productive day!!';
/*
        console.log("Getting Feedback ");
*/

  /*      var feedbackResults = [{"Timestamp":"Date(2015, 2, 3)","Feedback Date":"Date(2015, 2, 2)","Username":"itadmin@subhanu.com","Feedback_On":"chaitra","Feedback":"Filled up Old Subhanu Workday\nNot attentive during meeting.\nMeeting Actions not followed","Feedback_Action":"Be Attentive and make note of changes\nReview Notes after Meeting","Feedback_Rating":2,"Feedback_Category":"Process-Complaince","Feedback_Type":"Get-Better"}];
        $scope.feedbacks = this.feedbackResults;*/

       /* var queryUrl = getFeedbackMasterQueryURL('All',this.user,'','' );
        console.log("queryUrl -> " + queryUrl);
        var query = new google.visualization.Query(queryUrl);

        // Send the query with a callback function.
        query.send(function handleQueryResponse(response) {
            if (response.isError()) {
                alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }
            data = response.getDataTable();
            var jsonData = data.toJSON();
            var parsedJSONData = JSON.parse(jsonData);
            var formattedData = formatResult(parsedJSONData);
            console.log(formattedData);
            $scope.feedbacks = formattedData.ResultSet;
            console.log($scope.feedbacks);

            $scope.$apply(function () {
                console.log("Apply Called");
                $scope.activateUserName = true;
            });
        });*/

        //$scope.edit = function(allFeedbacks, rowIndex) {
        //    console.log("Edit " + rowIndex);
        //    console.log("Edit " + allFeedbacks);
        //    $rootScope.feedbackDetails=$scope.feedbacks[rowIndex];
        //   // console.log("Rootscope " + $rootScope.feedbackDetails.feedback);
        //}

        $scope.activateUserName = true;

    }

};