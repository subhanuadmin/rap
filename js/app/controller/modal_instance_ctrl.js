/**
 * Created by Rajesh_Krishnan1 on 4/6/2015.
 */
function ModalInteractionDetailsCtrl ($scope,$rootScope, $modalInstance) {

    console.log("Entered ModalInteractionDetailsCtrl");

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.selectedStatus =  $rootScope.selectedStatus;
    $scope.displayResults = $rootScope.statusResults;
    $scope.rangeSize = ($scope.displayResults).length;

    var range = [];
    for(var i=0;i<$scope.rangeSize;i++) {
        range.push(i);
    }
    $scope.range = range;

    console.log("Range size " + $scope.rangeSize);
    console.log($scope.displayResults);

};
