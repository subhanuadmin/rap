/**
 * Created by Rajesh_Krishnan on 2/24/2015.
 */


function ActivityMasterCtrl($scope,$window,$rootScope,notify) {
    console.log('ActivityMasterCtrl');

    $scope.loadActivityMasterForm = function () {
        console.log("Load Activity Tracker For");
        console.log($rootScope.trackerURL);
        if ($rootScope.loggedInUser == null){
            notify({
                message:'Please set user',
                classes: 'alert-danger'
            })
        } else {
            $window.open("https://drive.google.com/drive/folders/0BxYvrrMah1kAZWZfMkhfNGg3dlE", '_target');
            $window.open($rootScope.trackerURL, '_target');
        }

        console.log("After loadActivityMasterForm");
    };
}