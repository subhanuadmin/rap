/**
 * Created by DIS 12 on 11-04-2016.
 */
function MeetingReportCtrl($scope,$filter, $location, $rootScope, $window) {
    console.log("MeetingReportCtrl");
    $scope.activateMeetingTable = false;

    console.log("$rootScope.root_stored_token -> " + $rootScope.root_stored_token);

    $scope.meets =[{ "Timestamp": "2016-01-28T18:30:00.000Z",
        "Username": " ",
        "Meeting_Date": "",
        "Meeting_Type": "",
        "Attendees": "",
        "Meeting_Agenda": "",
        "Tasks_Actions": ""
    }, {
        "Timestamp": "2016-01-28T18:30:00.000Z",
        "Username": "",
        "Meeting_Date": "",
        "Meeting_Type": "",
        "Attendees": "",
        "Meeting_Agenda": "",
        "Tasks_Actions": ""
    }];

    $scope.$watch('activateMeetingTable', function () {
        // alert('hey, myVar has changed!');
    });

    $scope.getMeetingReportDetails = function () {
        $scope.meetingReportLoadingStatus = "Loading";
        console.log("loadMeetingReport");

       // var startDateWithSlash = $filter('date')($scope.a, "yyyy/MM/dd");
      //  var endDateWithSlash = $filter('date')($scope.b, "yyyy/MM/dd");
        var startDateWithHyphen = $filter('date')($scope.a, "yyyy-MM-dd");
        var endDateWithHyphen = $filter('date')($scope.b, "yyyy-MM-dd");

        console.log("Selected - Start Date -> " + startDateWithHyphen );
        console.log("Selected - End Date -> " + endDateWithHyphen);
        console.log("Selected - RAP Developer -> " + $scope.RAPDeveloper);


        $scope.meets =[{ "Timestamp": "2016-01-28T18:30:00.000Z",
            "Username": "",
            "Meeting_Date": "",
            "Meeting_Type": "",
            "Attendees": "",
            "Meeting_Agenda": "",
            "Tasks_Actions": ""

        }, {
            "Timestamp": "2016-01-28T18:30:00.000Z",
            "Username": "",
            "Meeting_Date": "",
            "Meeting_Type": "",
            "Attendees": "",
            "Meeting_Agenda": "",
            "Tasks_Actions": ""
        }];

        var queryUrl = getMeetingReportURL($scope.RAPDeveloper,startDateWithHyphen,endDateWithHyphen,$rootScope.root_stored_token);
        console.log("queryUrl -> " + queryUrl);
        var query = new google.visualization.Query(queryUrl);


        // Send the query with a callback function.
        query.send(function handleQueryResponse(response) {
            if (response.isError()) {
                alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
                return;
            }
            data = response.getDataTable();
            var jsonData = data.toJSON();
            var parsedJSONData = JSON.parse(jsonData);
            var formattedData = formatResult(parsedJSONData);
            $scope.meets = formattedData.ResultSet;
            $scope.$apply(function () {
                console.log("Apply Called");
                $scope.meetingReportLoadingStatus = "";
                $scope.activateMeetingTable = true;
            });
        });
    };
}
