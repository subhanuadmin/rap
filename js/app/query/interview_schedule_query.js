/**
 * Created by Rajesh_Krishnan1 on 2/25/2015.
 */

function getInterviewScheduleQueryURL(requirementID,userName,fromDate,toDate,interviewRound,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1Ykb0HEwadmCdt8ANy7cDcb1XwLAPvNRZaqX3zkYdIBU/gviz/tq?tq=";
    var  columnSelect = "SELECT BO,BP,AK,AJ,AL ";
    var  whereClause = " WHERE ";
    var requirementStartClause = "D CONTAINS '";
//    var  requirementID = "" ;
    var requirementEndClause = "'";

    var  andClause = " AND ";
    var statusStartClause = "E CONTAINS '";
    var  statusID = "Client Interview Scheduled" ;
    var statusEndClause = "'";

    var userStartClause = "B CONTAINS '";
    //var  userName = "" ;
    var userEndClause = "'";

    var fromDateStartClause = "toDate(AK) >= date '";
   // var fromDate = "" ;
    var fromDateEndClause = "'";

    var toDateStartClause = "toDate(AK) <= date '";
   // var toDate = "" ;
    var toDateEndClause = "'";

    var startRoundClause = "AJ CONTAINS '";
    // var toDate = "" ;
    var endRoundClause = "'";

    var  queryEnd = " NOT(BO CONTAINS 'Duplicate') ORDER BY AK ASC";
    var datasourceUrlEnd = "&gid=2137357099&access_token="+stored_token;

    var queryURL = columnSelect + whereClause;

    if (requirementID != 'All'){
        console.log('Requirement Selected');
        queryURL = queryURL + requirementStartClause + requirementID +  requirementEndClause + andClause;
    }

    if (statusID != 'All' ){
        console.log('Status Condition');
        queryURL = queryURL + statusStartClause + statusID +  statusEndClause + andClause;
    }

    if (userName != 'All' ){
        console.log('User Selected');
        queryURL = queryURL + userStartClause + userName +  userEndClause + andClause;
    }
    
    if (fromDate != "" ){
        console.log('From Date Selected');
        queryURL = queryURL + fromDateStartClause + fromDate +  fromDateEndClause + andClause;
    }

    if (toDate != "" ){
        console.log(' To Date Selected');
        queryURL = queryURL + toDateStartClause  + toDate +  toDateEndClause + andClause;
    }

    if (interviewRound != 'All' ){
        console.log(' Interview Round Selected');
        queryURL = queryURL + startRoundClause + interviewRound +  endRoundClause + andClause;
    }
    queryURL = queryURL + queryEnd;

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
   // console.log(datasourceUrl);
    return datasourceUrl;
}

function getInternalInterviewScheduleQueryURL(requirementID,userName,fromDate,toDate,interviewRound,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1stde4_n_-qk6dkylDrT4k8AC4wV60qA_xOFecJ-VAXU/gviz/tq?tq=";
    var  columnSelect = "SELECT CJ,CK,R,CM,CN,CO,Q,CP,C,D ";
    var  whereClause = " WHERE ";
    var requirementStartClause = "D CONTAINS '";
//    var  requirementID = "" ;
    var requirementEndClause = "'";

    var  andClause = " AND ";
    var statusStartClause = "F CONTAINS '";
    var  statusID = "Client Interview Scheduled" ;
    var statusEndClause = "'";

    var userStartClause = "B CONTAINS '";
    //var  userName = "" ;
    var userEndClause = "'";

    var fromDateStartClause = "toDate(R) >= date '";
    // var fromDate = "" ;
    var fromDateEndClause = "'";

    var toDateStartClause = "toDate(R) <= date '";
    // var toDate = "" ;
    var toDateEndClause = "'";

    var startRoundClause = "Q CONTAINS '";
    // var toDate = "" ;
    var endRoundClause = "'";

    var  queryEnd = " NOT(CJ CONTAINS 'Duplicate') ORDER BY R ASC";
    var datasourceUrlEnd = "&gid=2144717285&access_token="+stored_token;

    var queryURL = columnSelect + whereClause;

    if (requirementID != 'All'){
        console.log('Requirement Selected');
        queryURL = queryURL + requirementStartClause + requirementID +  requirementEndClause + andClause;
    }

    if (statusID != 'All' ){
        console.log('Status Condition');
        queryURL = queryURL + statusStartClause + statusID +  statusEndClause + andClause;
    }

    if (userName != 'All' ){
        console.log('User Selected');
        queryURL = queryURL + userStartClause + userName +  userEndClause + andClause;
    }

    if (fromDate != "" ){
        console.log('From Date Selected');
        queryURL = queryURL + fromDateStartClause + fromDate +  fromDateEndClause + andClause;
    }

    if (toDate != "" ){
        console.log(' To Date Selected');
        queryURL = queryURL + toDateStartClause + toDate +  toDateEndClause + andClause;
    }

    if (interviewRound != 'All' ){
        console.log(' Interview Round Selected');
        queryURL = queryURL + startRoundClause + interviewRound +  endRoundClause + andClause;
    }

    queryURL = queryURL + queryEnd;

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
    // console.log(datasourceUrl);
    return datasourceUrl;

}
function getINRCInterviewScheduleQueryURL(requirementID,userName,fromDate,toDate,interviewRound,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1stde4_n_-qk6dkylDrT4k8AC4wV60qA_xOFecJ-VAXU/gviz/tq?tq=";
    var  columnSelect = "SELECT CN,CJ,CK,CV,CQ,CR,CS,CT,CU,CM,CO,CP";
    var  whereClause = " WHERE ";
    var requirementStartClause = "D CONTAINS '";
//    var  requirementID = "" ;
    var requirementEndClause = "'";

    var  andClause = " AND ";
    var statusStartClause = "F CONTAINS '";
    var  statusID = "Client Interview Scheduled" ;
    var statusEndClause = "'";

    var userStartClause = "B CONTAINS '";
    //var  userName = "" ;
    var userEndClause = "'";

    var fromDateStartClause = "toDate(R) >= date '";
    // var fromDate = "" ;
    var fromDateEndClause = "'";

    var toDateStartClause = "toDate(R) <= date '";
    // var toDate = "" ;
    var toDateEndClause = "'";

    var startRoundClause = "Q CONTAINS '";
    var endRoundClause = "'";

    var  queryEnd = " NOT(CJ CONTAINS 'Duplicate') ORDER BY R ASC";
    var datasourceUrlEnd = "&gid=2144717285&access_token="+stored_token;

    var queryURL = columnSelect + whereClause;

    if (requirementID != 'All'){
        console.log('Requirement Selected');
        queryURL = queryURL + requirementStartClause + requirementID +  requirementEndClause + andClause;
    }

    if (statusID != 'All' ){
        console.log('Status Condition');
        queryURL = queryURL + statusStartClause + statusID +  statusEndClause + andClause;
    }

    if (userName != 'All' ){
        console.log('User Selected');
        queryURL = queryURL + userStartClause + userName +  userEndClause + andClause;
    }

    if (fromDate != "" ){
        console.log('From Date Selected');
        queryURL = queryURL + fromDateStartClause + fromDate +  fromDateEndClause + andClause;
    }

    if (toDate != "" ){
        console.log(' To Date Selected');
        queryURL = queryURL + toDateStartClause + toDate +  toDateEndClause + andClause;
    }

    if (interviewRound != 'All' ){
        console.log(' Interview Round Selected');
        queryURL = queryURL + startRoundClause + interviewRound +  endRoundClause + andClause;
    }
    
    queryURL = queryURL + queryEnd;

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
    // console.log(datasourceUrl);
    return datasourceUrl;

}