/**
 * Created by snehita on 12/18/2015.
 */
function getMeetingReportURL(userName, fromDate, toDate,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1iv2jTKDyW3v877cqrXOgeAv6VTMTonltZ4MMetpoVPk/gviz/tq?tq=";
    var  columnSelect = "SELECT A,B,C,D,E,F,G ";
    var  whereClause = " WHERE ";
    var  andClause = " AND ";
    var  queryEnd = " NOT(B CONTAINS 'Username') ORDER BY A DESC  LABEL A 'Timestamp', B 'Username', C 'Meeting_Date', D 'Meeting_Type', E 'Attendees', F 'Meeting_Agenda', G 'Tasks_Actions'";
    //var datasourceUrlEnd = "&key=1stde4_n_-qk6dkylDrT4k8AC4wV60qA_xOFecJ-VAXU&gid=194951059";
    var datasourceUrlEnd = "&gid=143010577&access_token="+stored_token;
    //var query;

    var userStartClause = "B CONTAINS '";
    var userEndClause = "'";

    var fromDateStartClause = "toDate(A) >= date '";
    // var fromDate = "" ;
    var fromDateEndClause = "'";

    var toDateStartClause = "toDate(A) <= date '";
    // var toDate = "" ;
    var toDateEndClause = "'";

    var queryURL = columnSelect + whereClause;

    if (fromDate != "" ){
        console.log('From Date Selected');
        queryURL = queryURL + fromDateStartClause + fromDate +  fromDateEndClause + andClause;
    }

    if (toDate != "" ){
        console.log('To Date Selected');
        queryURL = queryURL + toDateStartClause + toDate +  toDateEndClause + andClause;
    }

    if (userName != 'All' ){
        console.log('User Selected');
        queryURL = queryURL + userStartClause + userName +  userEndClause + andClause;
    }

    queryURL = queryURL + queryEnd;

    //var queryURL = columnSelect + whereClause;
    //queryURL = queryURL + queryEnd;

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
    console.log(datasourceUrl);
    return datasourceUrl;
}
