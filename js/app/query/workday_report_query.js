/**
 * Created by DIS 12 on 22-04-2016.
 */

function getWorkdayReportQueryURL(userName, fromDate, toDate,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1PTV1nO95JJNhpGMo6yYUj8y_zB75nGITSZH8bw2nO7E/gviz/tq?tq=";
    var  columnSelect = "SELECT A,B,C,D,E,F";
    var  whereClause = " WHERE ";
    var  andClause = " AND ";
    var  queryEnd = " OR A CONTAINS 'holiday') AND  NOT(A CONTAINS 'Username') ORDER by C,A,D";
    //var datasourceUrlEnd = "&key=1-kaFkR2d96uutTzRvSTbLwuRd6fD5b15M7-K23OwFkQ&gid=1106745654";
    var datasourceUrlEnd = "&gid=858908302&access_token="+stored_token;

    var userStartClause = "(A CONTAINS '";
    var userEndClause = "'";

    var fromDateStartClause = "toDate(C) >= date '";
    // var fromDate = "" ;
    var fromDateEndClause = "'";

    var toDateStartClause = "toDate(C) <= date '";
    // var toDate = "" ;
    var toDateEndClause = "'";

    var queryURL = columnSelect + whereClause;

    if (fromDate != "" ){
        console.log('From Date Selected');
        queryURL = queryURL + fromDateStartClause + fromDate +  fromDateEndClause + andClause;
    }

    if (toDate != "" ){
        console.log(' To Date Selected');
        queryURL = queryURL + toDateStartClause + toDate +  toDateEndClause + andClause;
    }

    if (userName != 'All' ){
        console.log('User Selected');
        queryURL = queryURL + userStartClause + userName +  userEndClause ;
    }

    queryURL = queryURL + queryEnd;

    console.log(queryURL);

    var encodedQueryURI = encodeURIComponent(queryURL);
    console.log(encodedQueryURI);

    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
    // console.log(datasourceUrl);
    return datasourceUrl;
}