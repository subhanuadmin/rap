/**
 * Created by DIS 12 on 19-04-2016.
 */
function getActivityReportURL(fromDate, toDate, userName,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1HPRrQChk3VDQgjWYojwTeBHeVdyRyVgYMdkvpf43MP0/gviz/tq?tq=";
    var  columnSelect = "SELECT A,B,C,D,G,H";
    var  whereClause = " WHERE ";
    var andClause= " AND ";
    //var  queryEnd = " NOT(A CONTAINS 'Username') ORDER by C,A,D ";
    var  queryEnd = " NOT(B CONTAINS 'Username') ORDER BY A DESC LABEL A 'Timestamp', B 'Username', C 'Module_Name', D 'Activity_Category', G 'Activity_Comments', H 'Time_Spent'";
    var datasourceUrlEnd = "&gid=1440648868&access_token="+stored_token;

    var query;

    var userStartClause = " B CONTAINS '";
    var userEndClause = "'";

    var fromDateStartClause = "toDate(A) >= date '";
    // var fromDate = "" ;
    var fromDateEndClause = "'";

    var toDateStartClause = "toDate(A) <= date '";
    // var toDate = "" ;
    var toDateEndClause = "'";

    var queryURL = columnSelect + whereClause;

    if (fromDate != "" ){
        console.log('From Date Selected');
        queryURL = queryURL + fromDateStartClause + fromDate +  fromDateEndClause + andClause;
    }

    if (toDate != "" ){
        console.log('To Date Selected');
        queryURL = queryURL + toDateStartClause + toDate +  toDateEndClause + andClause;
    }

    if (userName != 'All' ){
        console.log('User Selected');
        queryURL = queryURL + userStartClause + userName +  userEndClause + andClause;
    }

    queryURL = queryURL + queryEnd;



/*

    var queryURL = columnSelect + whereClause;
    queryURL = queryURL + queryEnd;

*/

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
    console.log(datasourceUrl);
    return datasourceUrl;
}
