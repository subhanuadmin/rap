/**
 * Created by Rajesh_Krishnan1 on 2/25/2015.
 */

function getDailyMetricsMasterQueryURL(stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1TEmh17CorZT-V_xdmqpPRUBhZmQbRwM57qhbxedjOTs/gviz/tq?tq=";
    //var  columnSelect = "SELECT A";
    var  columnSelect = "SELECT A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P";
    var  whereClause = " WHERE ";
    var  queryEnd = " NOT(B CONTAINS 'Internal Activities') ORDER BY A DESC";
    var datasourceUrlEnd = "&gid=1374833313&access_token="+stored_token;

    var queryURL = columnSelect + whereClause;
    //var queryURL = columnSelect;
    queryURL = queryURL + queryEnd;

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
   // console.log(datasourceUrl);
    return datasourceUrl;
}