/**
 * Created by Rajesh_Krishnan1 on 2/25/2015.
 */

function getCandidateInteractionQueryURL(requirementID,candidateID,userName,startDate,toDate,candidateStatus,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1Ykb0HEwadmCdt8ANy7cDcb1XwLAPvNRZaqX3zkYdIBU/gviz/tq?tq=";
    var  columnSelect = "SELECT A,B,C,D,E,F,G,BO,BP,BK ";
    var  whereClause = " WHERE ";
    var requirementStartClause = "D CONTAINS '";
//    var  requirementID = "" ;
    var requirementEndClause = "'";

    var userStartClause = "B = '";
    //var  userName = "" ;
    var userEndClause = "'";


    var  queryEnd = " C CONTAINS 'CAND' AND NOT(BO CONTAINS 'Duplicate') ORDER BY E DESC LABEL A 'Timestamp', B 'Username', C 'Candidate_ID', D 'Requirement_ID', E 'Candidate_Status', F 'Interaction_Date', G 'Interaction_Comments', BO 'Name', BP 'Contact_Number', BK 'Time_Spent' ";

    var datasourceUrlEnd = "&gid=2137357099&access_token="+stored_token;
   // var datasourceUrlEnd = "&key=1stde4_n_-qk6dkylDrT4k8AC4wV60qA_xOFecJ-VAXU&gid=2144717285";
   // var datasourceUrlEnd = "&key=1stde4_n_-qk6dkylDrT4k8AC4wV60qA_xOFecJ-VAXU&gid=2144717285";
   // var datasourceUrlEnd = "&key=1stde4_n_-qk6dkylDrT4k8AC4wV60qA_xOFecJ-VAXU&gid=2144717285";
    var query;


   var queryURL = columnSelect + whereClause;

    if (requirementID != 'All'){
        console.log('Requirement Selected');
        queryURL = queryURL + requirementStartClause + requirementID +  requirementEndClause + andClause;
    }


    if (userName != 'All' ){
        console.log('User Selected');
        queryURL = queryURL + userStartClause + userName +  userEndClause + andClause;
    }

    queryURL = queryURL + queryEnd;

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
   // console.log(datasourceUrl);
    return datasourceUrl;
}

function getAllInteractionDetailsQueryURL(requirementID,candidateID,userName,startDate,toDate,candidateStatus,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1Ykb0HEwadmCdt8ANy7cDcb1XwLAPvNRZaqX3zkYdIBU/gviz/tq?tq=";
/*    var  columnSelect = "SELECT A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,X,Y,Z " +
                        ", AA,AB,AC,AD,AE,AF,AG,AH,AI,AJ,AK,AL,AM,AN,AO,AP,AQ,AR,AS,AT,AU,AV,AX,AY,AZ" +
                        ", BA,BB,BC,BD,BE,BF,BG,BH,BI,BJ,BK,BL,BM,BN,BO,BP,BQ,BR,BS,BT,BU,BV,BX"+
                        " ,CE,CF,CG ";*/

    var  columnSelect = "SELECT A,B,C,D,E,F,G,BO,BP,BK";
    var  whereClause = " WHERE ";
    var  andClause = " AND ";
    var requirementStartClause = "D CONTAINS '";
//    var  requirementID = "" ;
    var requirementEndClause = "'";

    var candidateStartClause = "C = '";
//    var  requirementID = "" ;
    var candidateEndClause = "'";

    var userStartClause = "B CONTAINS '";
    //var  userName = "" ;
    var userEndClause = "'";

    var fromDateStartClause = "toDate(A) >= date '";
    // var fromDate = "" ;
    var fromDateEndClause = "'";

    var toDateStartClause = "toDate(A) <= date '";
    // var toDate = "" ;
    var toDateEndClause = "'";


    var  queryEnd = " C CONTAINS 'CAND'  ORDER BY E DESC LABEL A 'Timestamp', B 'Username', C 'Candidate_ID', D 'Requirement_ID', E 'Candidate_Status', F 'Interaction_Date', G 'Interaction_Comments', BO 'Name', BP 'Contact_Number', BK 'Time_Spent' ";

    var datasourceUrlEnd = "&gid=2137357099&access_token="+stored_token;
    //var datasourceUrlEnd = "&key=1stde4_n_-qk6dkylDrT4k8AC4wV60qA_xOFecJ-VAXU&gid=2144717285";

    var query;


    var queryURL = columnSelect + whereClause;

    if (requirementID != 'All'){
       // console.log('Requirement Selected');
        queryURL = queryURL + requirementStartClause + requirementID +  requirementEndClause + andClause;
    }

    if (candidateID != ''){
       // console.log('Candidate Selected');
        queryURL = queryURL + candidateStartClause + candidateID +  candidateEndClause + andClause;
    }

    if (userName != 'All' ){
       // console.log('User Selected');
        queryURL = queryURL + userStartClause + userName +  userEndClause + andClause;
    }

    if (startDate != "" ){
       // console.log('From Date Selected');
        queryURL = queryURL + fromDateStartClause + startDate +  fromDateEndClause + andClause;
    }

    if (toDate != "" ){
       // console.log(' To Date Selected');
        queryURL = queryURL + toDateStartClause + toDate +  toDateEndClause + andClause;
    }

    queryURL = queryURL + queryEnd;

    console.log(queryURL);
    var encodedQueryURI = encodeURIComponent(queryURL);
    console.log(encodedQueryURI);
    var datasourceUrl = datasourceUrlStart +encodedQueryURI +datasourceUrlEnd;
    // console.log(datasourceUrl);
    return datasourceUrl;
}