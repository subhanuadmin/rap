/**
 * Created by Malini on 12-Mar-15.
 */
 function getOfferAcceptedQueryURL(requirementID,userName,fromDate,toDate,stored_token) {

    var datasourceUrlStart = "https://docs.google.com/spreadsheets/d/1Ykb0HEwadmCdt8ANy7cDcb1XwLAPvNRZaqX3zkYdIBU/gviz/tq?tq=";
    var  columnSelect = "SELECT D,C,BO,BP,AU,AV,AW ";
    var  whereClause = " WHERE ";
    var requirementStartClause = "D = '";
  //  var  requirementID = "" ;
    var requirementEndClause = "'";


    var  andClause = " AND ";
    var statusStartClause = "E CONTAINS '";
    var  statusID = "Offer Accepted" ;
    var statusEndClause = "'";

    var userStartClause = "B = '";
   // var  userName = "" ;
    var userEndClause = "'";

    var fromDateStartClause = "toDate(AU) >= date '";
  //  var fromJoiningDate = "" ;
    var fromDateEndClause = "'";

    var toDateStartClause = "toDate(AU) <= date '";
   // var toJoiningDate = "" ;
    var toDateEndClause = "'";

    var  queryEnd = " NOT (BO CONTAINS 'Duplicate' OR BO CONTAINS 'Test') ORDER BY AU ASC";
    var datasourceUrlEnd = "&gid=2137357099&access_token="+stored_token;
    var query;


     var queryURL = columnSelect + whereClause;

        if (requirementID != 'All') {
            console.log('Requirement Selected');
            queryURL = queryURL + requirementStartClause + requirementID + requirementEndClause + andClause;
        }

        if (statusID != 'All') {
            console.log('Status Condition');
            queryURL = queryURL + statusStartClause + statusID + statusEndClause + andClause;
        }

        if (userName != 'All') {
            console.log('User Selected');
            queryURL = queryURL + userStartClause + userName + userEndClause + andClause;
        }

        if (fromDate != "") {
            console.log('From Date Selected');
            queryURL = queryURL + fromDateStartClause + fromDate + fromDateEndClause + andClause;
        }

        if (toDate != "") {
            console.log(' To Date Selected');
            queryURL = queryURL + toDateStartClause + toDate + toDateEndClause + andClause;
        }
        queryURL = queryURL + queryEnd;

        console.log(queryURL);
        var encodedQueryURI = encodeURIComponent(queryURL);
        console.log(encodedQueryURI);
        var datasourceUrl = datasourceUrlStart + encodedQueryURI + datasourceUrlEnd;
        console.log(datasourceUrl);
        return datasourceUrl;
    }