/**
 * INSPINIA - Responsive Admin Theme
 * Copyright 2015 Webapplayers.com
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {
    $urlRouterProvider.otherwise("/index/main");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider

        .state('index', {
            abstract: true,
            url: "/index",
            templateUrl: "views/common/content.html"
        })
        .state('index.main', {
            url: "/main",
            templateUrl: "views/main.html",
            data: { pageTitle: 'Subhanu Application Pro' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngGrid',
                            files: ['js/plugins/nggrid/ng-grid-2.0.3.min.js']
                        },
                        {
                            insertBefore: '#loadBefore',
                            files: ['js/plugins/nggrid/ng-grid.css']
                        }
                    ]);
                }
            }
        })
        .state('index.feedback_details', {
            url: "/feedbackDetails",
            templateUrl: "views/feedback_details.html",
            data: { pageTitle: 'Feedback Details' }
        })
        .state('forms', {
            abstract: true,
            url: "/recruitmentForms",
            templateUrl: "views/common/content.html"
        })
        .state('forms.candidate_register', {
            url: "/candidateRegister",
            templateUrl: "views/forms/candidate_register_form.html",
            data: {pageTitle: 'Candidate Registration'}
        })
        .state('forms.candidate_status', {
            url: "/candidateStatus",
            templateUrl: "views/forms/candidate_status_form.html",
            data: {pageTitle: 'Candidate Interaction/Status'}
        })
        .state('forms.daily_metrics', {
            url: "/dailyMetrics",
            templateUrl: "views/forms/daily_metrics_form.html",
            data: {pageTitle: 'Daily Metrics'}
        })
        .state('forms.documents_update', {
            url: "/documentsUpdate",
            templateUrl: "views/forms/document_update_form.html",
            data: {pageTitle: 'Documents Update'}
        })
        .state('forms.publish_posting', {
            url: "/publishPosting",
            templateUrl: "views/forms/publish_posting_form.html",
            data: {pageTitle: 'Publish Posting'}
        })
        .state('forms.client_interaction', {
            url: "/clientInteraction",
            templateUrl: "views/recruitment_forms/client_interaction_form.html",
            data: {pageTitle: 'Client Interaction'}
        })
        .state('iforms', {
            abstract: true,
            url: "/internalForms",
            templateUrl: "views/common/content.html"
        })
        .state('iforms.employee_register', {
            url: "/employeeRegister",
            templateUrl: "views/forms/employee_register_form.html",
            data: {pageTitle: 'Employee Register'}
        })
        .state('iforms.meeting_minutes', {
            url: "/meetingMinutes",
            templateUrl: "views/forms/meeting_minutes_form.html",
            data: {pageTitle: 'Meeting Minutes'}
        })
        .state('iforms.internal_feedback', {
            url: "/internalFeedback",
            templateUrl: "views/forms/internal_feedback_form.html",
            data: {pageTitle: 'Internal Feedback'}
        })
        .state('iforms.activity_tracker', {
            url: "/activityTracker",
            templateUrl: "views/forms/activity_tracker_form.html",
            data: {pageTitle: 'Activity Tracker'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        }
                    ]);
                }
            }
        })
        .state('lookup', {
            abstract: true,
            url: "/lookup",
            templateUrl: "views/common/content.html"
        })
        .state('lookup.candidate_master', {
            url: "/candidateMaster",
            templateUrl: "views/lookup/candidate_master.html"
        })
        .state('lookup.candidate_details', {
            url: "/candidateDetails",
            templateUrl: "views/lookup/candidate_details.html"
        })
        .state('lookup.candidates_interaction', {
            url: "/candidateInteraction",
            templateUrl: "views/lookup/candidate_interaction_master.html"
        })
        .state('lookup.candidates_interaction_details', {
            url: "/candidateInteractionDetails",
            templateUrl: "views/lookup/candidate_interaction_details.html"
        })
        .state('lookup.requirement_master', {
            url: "/requirementMaster",
            templateUrl: "views/lookup/requirement_master.html"
        })
        .state('lookup.requirement_details', {
            url: "/requirementDetails",
            templateUrl: "views/lookup/requirement_details.html"
        })
        .state('lookup.daily_metrics_master', {
            url: "/dailyMetricsMaster",
            templateUrl: "views/lookup/daily_metrics_master.html"
        })
        .state('lookup.daily_metrics_details', {
            url: "/dailyMetricsDetails",
            templateUrl: "views/lookup/daily_metrics_details.html"
        })
        .state('lookup.employee_master', {
            url: "/employeeMaster",
            templateUrl: "views/lookup/employee_master.html"
        })
        .state('lookup.employee_details', {
            url: "/employeeDetails",
            templateUrl: "views/lookup/employee_details.html"
        })
        .state('dashboard', {
            abstract: true,
            url: "/dashboard",
            templateUrl: "views/common/content.html",
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/datePicker.js']
                        }
                    ]);
                }
            }
        })
        .state('dashboard.requirement_summary', {
            url: "/requirementSummary",
            templateUrl: "views/dashboard/requirement_summary.html"
        })
        .state('reports', {
            abstract: true,
            url: "/reports",
            templateUrl: "views/common/content.html",
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/datePicker.js']
                        }
                    ]);
                }
            }
        })
        .state('reports.interview_schedule_report', {
            url: "/interviewSchedule",
            templateUrl: "views/reports/interview_schedule_report.html"
        })
        .state('reports.joined_candidates_report', {
            url: "/joinedCandidates",
            templateUrl: "views/reports/joined_candidates_report.html"
        })
        .state('reports.offer_accepted_candidates_report', {
            url: "/offerAcceptedCandidates",
            templateUrl: "views/reports/offer_accepted_candidates_report.html"
        })
        .state('reports.email_candidate_summary', {
            url: "/emailCandidateSummary",
            templateUrl: "views/reports/email_candidate_summary.html",
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/plugins/summernote/summernote.css','css/plugins/summernote/summernote-bs3.css','js/plugins/summernote/summernote.min.js']
                        },
                        {
                            name: 'summernote',
                            files: ['css/plugins/summernote/summernote.css','css/plugins/summernote/summernote-bs3.css','js/plugins/summernote/summernote.min.js','js/plugins/summernote/angular-summernote.min.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css','js/plugins/iCheck/icheck.min.js']
                        }
                    ]);
                }
            }
        })
         .state('status_reports', {
            abstract: true,
            url: "/statusReports",
            templateUrl: "views/common/content.html",
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/datePicker.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        }
                    ]);
                }
            }
        })
        .state('status_reports.daily', {
            url: "/dailyStatus",
            templateUrl: "views/status_reports/daily_status.html"
        })
        .state('status_reports.weekly', {
            url: "/weeklyStatus",
            templateUrl: "views/status_reports/weekly_status.html"
        })
        .state('workday', {
            abstract: true,
            url: "/workday",
            templateUrl: "views/common/content.html",
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/datePicker.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        }
                    ]);
                }
            }
        })
        .state('workday.workday_form', {
            url: "/workdayForm",
            templateUrl: "views/workday/workday_form.html"
        })
        .state('workday.workday_report', {
            url: "/workdayReport'",
            templateUrl: "views/workday/workday_master.html"
        })
        .state('workday.leave_plan', {
            url: "/leavePlan",
            templateUrl: "views/workday/leave_plan.html"
        })
        .state('workday.paybooks', {
            url: "/workdayReport'",
            templateUrl: "views/workday/paybooks_leave.html"
        })
        .state('workday.policy', {
            url: "/subhanuPolicy'",
            templateUrl: "views/workday/subhanu_policy.html"
        })
        .state('workday.roles', {
            url: "/subhanuRoles'",
            templateUrl: "views/workday/subhanu_responsibilities.html"
        })
        .state('subhanuInfo', {
            abstract: true,
            url: "/subhanuInfo",
            templateUrl: "views/common/content.html",
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/datePicker.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        }
                    ]);
                }
            }
        })
        .state('subhanuInfo.teamcontact', {
            url: "/teamContacts",
            templateUrl: "views/subhanu_info/team_contact.html"
        })
        .state('subhanuInfo.aboutSubhanu', {
            url: "/aboutSubhanu",
            templateUrl: "views/subhanu_info/about_subhanu.html"
        })
        .state('subhanuInfo.training', {
            url: "/subhanuTraining",
            templateUrl: "views/subhanu_info/subhanu_training.html"
        })
}
angular
    .module('inspinia')
    .config(config)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });
