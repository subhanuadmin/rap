/**
 * INSPINIA - Responsive Admin Theme
 * Copyright 2015 Webapplayers.com
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {
    $urlRouterProvider.otherwise("/index/main");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider

        .state('index', {
            abstract: true,
            url: "/index",
            templateUrl: "views/common/content.html"
        })
        .state('index.main', {
            url: "/main",
            templateUrl: "views/main.html",
            data: {pageTitle: 'Rapidd Application Pro'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngGrid',
                            files: ['js/plugins/nggrid/ng-grid-2.0.3.min.js']
                        },
                        {
                            insertBefore: '#loadBefore',
                            files: ['js/plugins/nggrid/ng-grid.css']
                        }
                    ]);
                }
            }
        })
        .state('index.feedback_details', {
            url: "/feedbackDetails",
            templateUrl: "views/feedback_details.html",
            data: {pageTitle: 'Feedback Details'}
        })


        .state('iforms', {
            abstract: true,
            url: "/internalForms",
            templateUrl: "views/common/content.html"
        })
        .state('iforms.meeting_minutes', {
            url: "/meetingMinutes",
            templateUrl: "views/forms/meeting_minutes_form.html",
            data: {pageTitle: 'Meeting Minutes'}
        })

        .state('iforms.activity_tracker', {
            url: "/activityTracker",
            templateUrl: "views/forms/activity_tracker_form.html",
            data: {pageTitle: 'Activity Tracker'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css', 'js/plugins/angular-notify/angular-notify.min.js']
                        }
                    ]);
                }
            }
        })
        .state('iforms.documents_update', {
            url: "/documentsUpdate",
            templateUrl: "views/forms/document_update_form.html",
            data: {pageTitle: 'Documents Update'}
        })

        .state('status_reports', {
            abstract: true,
            url: "/statusReports",
            templateUrl: "views/common/content.html",
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css', 'js/plugins/datapicker/datePicker.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css', 'js/plugins/angular-notify/angular-notify.min.js']
                        }
                    ]);
                }
            }
        })

        .state('status_reports.tasks', {
            url: "/taskStatus",
            templateUrl: "views/status_reports/task_status.html"
        })
        .state('status_reports.feedback', {
            url: "/feedbackStatus",
            templateUrl: "views/status_reports/feedback_status.html"
        })

        .state('status_reports.meeting_report', {
            url: "/meetingReport",
            templateUrl: "views/status_reports/meeting_report.html"
        })

        .state('status_reports.activity_report', {
            url: "/activityReport",
            templateUrl: "views/status_reports/activity_report.html"
        })



        .state('workday', {
            abstract: true,
            url: "/workday",
            templateUrl: "views/common/content.html",
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css', 'js/plugins/datapicker/datePicker.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css', 'js/plugins/angular-notify/angular-notify.min.js']
                        }
                    ]);
                }
            }
        })
        .state('workday.workday_form', {
            url: "/workdayForm",
            templateUrl: "views/workday/workday_form.html"
        })
        .state('workday.workday_report', {
            url: "/workdayReport",
            templateUrl: "views/workday/workday_report.html"
        })

        .state('workday.leave_plan', {
            url: "/leavePlan",
            templateUrl: "views/workday/leave_plan.html"
        })

}
    angular
        .module('inspinia')
        .config(config)
        .run(function ($rootScope, $state) {
            $rootScope.$state = $state;
        });

