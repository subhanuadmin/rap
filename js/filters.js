'use strict';

angular.module('splitFilter', [])
    .filter('split', function() {
        return function(input, splitChar, splitIndex) {
            // do some bounds checking here to ensure it has that index
            return input.split(splitChar)[splitIndex];
        }
    });

angular.module('specialCharacterFilter', [])
    .filter('getName', function() {
        return function(input, splitChar, splitIndex) {
            // do some bounds checking here to ensure it has that index
            var name = input.split(splitChar)[splitIndex];
            return name.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
        }
    });

angular.module('dateConverter', [])
    .filter('formatDate', function() {
        return function(input, reqid) {
            // do some bounds checking here to ensure it has that index
            //console.log("reqid -> " + reqid);
            //console.log("Date Formatter -> " + input);
        if(input != null){
            var indexOfFirstBrace = input.indexOf("(");
            var indexOfFirstComma = input.indexOf(",");
            var indexOfSecondComma = input.indexOf(",",indexOfFirstComma+1);
            var indexOfLastBrace = input.indexOf(")");

            var year = input.substring(indexOfFirstBrace+1,indexOfFirstComma);
            var month = input.substring(indexOfFirstComma+1,indexOfSecondComma);
            // console.log("month-> " + month);
            if(month != ''){
                month = Number(month) + 1;
            }
            var day = input.substring(indexOfSecondComma+1,indexOfLastBrace);

            var strDate = month + "/" + day + "/" + year;
            //console.log("strDate-> " + strDate);
            if( strDate == '//'){
                strDate = '0/0/0';
                //console.log("Blank Date" + strDate);
            }
            var formattedDate = new Date(strDate);
            //console.log("formattedDate-> " + formattedDate);
            return formattedDate;
        }

        }
    })
    .filter('formatGoogleDate', function() {
        return function(input) {
            // do some bounds checking here to ensure it has that index
            //console.log("reqid -> " + reqid);
            //console.log("Date Formatter -> " + input);
            if (input != null) {
                var indexOfFirstBrace = input.indexOf("(");
                var indexOfFirstComma = input.indexOf(",");
                var indexOfSecondComma = input.indexOf(",", indexOfFirstComma + 1);
                var indexOfThirdComma = input.indexOf(",", indexOfSecondComma + 1);
                var indexOfFourthComma = input.indexOf(",", indexOfThirdComma + 1);
                var indexOfFifthComma = input.indexOf(",", indexOfFourthComma + 1);

                var indexOfLastBrace = input.indexOf(")");

                var year = input.substring(indexOfFirstBrace + 1, indexOfFirstComma);
                var month = input.substring(indexOfFirstComma + 1, indexOfSecondComma);
                // console.log("month-> " + month);
                if (month != '') {
                    month = Number(month) + 1;
                }
                var day = input.substring(indexOfSecondComma + 1, indexOfThirdComma);

                if (strDate == '//') {
                    strDate = '0/0/0';
                    //console.log("Blank Date" + strDate);
                }
                var hour = input.substring(indexOfThirdComma + 1, indexOfFourthComma);
                //console.log("Hour-> " + hour);

                var minute = input.substring(indexOfFourthComma + 1, indexOfFifthComma);
                //console.log("minute-> " + minute);

                var second = input.substring(indexOfFifthComma + 1, indexOfLastBrace);
                //console.log("second-> " + second);

                var strDate = month + "/" + day + "/" + year + " " + hour + ":" + minute + ":" + second;
                //console.log("strDate-> " + strDate);

                var formattedDate = new Date(strDate);
                //console.log("formattedDate-> " + formattedDate);
                return formattedDate.toLocaleDateString() + " " + formattedDate.toLocaleTimeString();
            }
        }
    });
angular.module('splitNewline', [])
    .filter('splitAtNewline', function() {
        return function(text, length, end) {
            if (text) {
                return text.split('\n ').join('<br>');
            }
        }
});

